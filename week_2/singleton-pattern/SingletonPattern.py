class OnlyOne:
    class __OnlyOne:
        def __init__(self, arg):
            self.val = arg
        def __str__(self):
            return repr(self) + self.val
    instance = None
    def __init__(self, arg):
        if not OnlyOne.instance:
            print('inisiasi sebuah instance')
            OnlyOne.instance = OnlyOne.__OnlyOne(arg)
        else:
            print('update di instance yang sama')
            OnlyOne.instance.val = arg
    def __getattr__(self, name):
        return getattr(self.instance, name)

def main():
    x = OnlyOne('sausage')
    print(x)
    print(x.instance)
    print()
    y = OnlyOne('eggs')
    print(y)
    print(y.instance)
    print()
    z = OnlyOne('spam')
    print(z)
    print(z.instance)
    print()

    print('Kembali melihat object x')
    print(x)
    print(x.instance)
    print()

    print('Kembali melihat object y')
    print(y)
    print(y.instance)
    print()

if __name__ == "__main__":
    main()