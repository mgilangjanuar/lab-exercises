#	Mandatory 3: Create a program so it implement Factory Method Pattern. There are a template class to use Factory Method Pattern but you are free to explore your idea in implementing Factory Method Pattern.

CIRCLE, CROSS, CIRCLE_ALT, CROSS_ALT = ("o", "x", "v", "u")

def main():
	board = Board(8)
	print (board)

class AbstractBoard:
	
	def __init__(self, rows, columns):
		self.board = [[None for _ in range(columns)] for _ in range(rows)]
		self.populate_board()
		
	def populate_board(self):
		raise NotImplementedError()

	def __str__(self):
		squares = []
		for x, row in enumerate(self.board):
			for y, column in enumerate(self.board):
				squares.append(self.board[x][y])
			squares.append("\n")
		return "".join(squares)

class Board(AbstractBoard):
	def __init__(self, n=3):
		self.n = n
		super().__init__(n, n)

	def populate_board(self):
		for row in range(self.n):
			for column in range(self.n):
				if (column % 2): self.board[row][column] = create_piece(CIRCLE_ALT if row % 2 else CIRCLE)
				else: self.board[row][column] = create_piece(CROSS_ALT if row % 2 else CROSS)
		

class Board3x3(AbstractBoard):
	def __init__(self):
		super().__init__(3, 3)
		
	def populate_board(self):
		for row in range(3):
			for column in range(3):
				if (column % 2): self.board[row][column] = create_piece(CIRCLE)
				else: self.board[row][column] = create_piece(CROSS)

def create_piece(symbol):
	if symbol == CIRCLE:
		return Circle()
	elif symbol == CROSS:
		return Cross()
	elif symbol == CIRCLE_ALT:
		return Circle(CIRCLE_ALT)
	elif symbol == CROSS_ALT:
		return Cross(CROSS_ALT)
	raise NotImplementedError()

class Piece(str):
	__slots__ = ()

	def __new__(cls):
		return str.__new__(cls, cls.__slots__)

class Circle(Piece):
	def __new__(cls, slots=CIRCLE):
		cls.__slots__ = slots
		return super().__new__(cls)

class Cross(Piece):
	def __new__(cls, slots=CROSS):
		cls.__slots__ = slots
		return super().__new__(cls)

if __name__ == "__main__":
    main()
				
