def main():
    redCircle = Circle(100, 100, 10, RedCircle())
    greenCircle = Circle(100, 100, 10, GreenCircle())

    redCircle.draw()
    greenCircle.draw()
       
class DrawAPI:

    def drawCircle(self, radius, x, y):
        pass

class Shape:

    def __init__(self, drawAPI):
        self.drawAPI = drawAPI

    def draw():
        pass

class Circle(Shape):

    def __init__(self, x, y, radius, drawAPI):
        super().__init__(drawAPI)
        self.x = x
        self.y = y
        self.radius = radius
        
    def draw(self):
        self.drawAPI.drawCircle(self.radius, self.x, self.y)
      
class RedCircle(DrawAPI):
    
    def drawCircle(self, radius, x, y):
        print("Drawing Circle[ color: red, radius: " +
                            str(radius) + ", x: " + str(x) + ", " + str(y) + "]")
        
class GreenCircle(DrawAPI):
    
    def drawCircle(self, radius, x, y):
        print("Drawing Circle[ color: green, radius: " +
                            str(radius) + ", x: " + str(x) + ", " + str(y) + "]")

if __name__ == "__main__":
    main()