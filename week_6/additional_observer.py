class SubscriberOne:

    def __init__(self, name):
        self.name = name

    def update(self, message):
        print('{} got message "{}"'.format(self.name, message))


class SubscriberTwo:

    def __init__(self, name):
        self.name = name

    def receive(self, message):
        print('{} got message "{}"'.format(self.name, message))


class Publisher:

    def __init__(self):
        self.subscribers = dict()

    def register(self, who, callback=None):
        self.subscribers[who] = callback

    def unregister(self, who):
        self.subscribers.pop(who)

    def notify(self, message):
        for subscriber in self.subscribers:
            if subscriber.__class__.__name__ == 'SubscriberOne':
                subscriber.update(message)
            else:
                subscriber.receive(message)
