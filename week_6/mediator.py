import random
import time


class TestController:

    def __init__(self):
        self._testmanager = None
        self._bProblem = 0

    def setup(self):
        print("Setting up the Test")
        time.sleep(0.1)
        self._testmanager.prepareReporting()

    def execute(self):
        self._testmanager._database.insert()
        print('Executing the test')

    def tearDown(self):
        print('Tearing down')
        self._testmanager._database.update()
        self._testmanager.publishReport()

    def setTM(self, testmanager):
        self._testmanager = testmanager

    def setProblem(self, value):
        self._bProblem = value


class Reporter:

    def __init__(self):
        self._testmanager = None

    def prepare(self):
        print("Reporter Class is preparing to report the results")
        time.sleep(0.1)

    def report(self):
        print("Reporting the results of Test")
        time.sleep(0.1)

    def setTM(self, testmanager):
        self._testmanager = testmanager


class Database:

    def __init__(self):
        self._testmanager = None

    def insert(self):
        print("Inserting the execution begin status in the Database ")
        time.sleep(0.1)
        if random.randrange(1, 4) == 3:
            return -1

    def update(self):
        print("Updating the test results in Database")
        time.sleep(0.1)

    def setTM(self, testmanager):
        self._testmanager = testmanager


class TestManager:

    def __init__(self):
        self._database = None
        self._reporter = None
        self._testcontroller = None

    def prepareReporting(self):
        self._reporter.prepare()

    def setReporter(self, reporter):
        self._reporter = reporter

    def setDB(self, database):
        self._database = database

    def publishReport(self):
        self._reporter.report()

    def setTC(self, testcontroller):
        self._testcontroller = testcontroller


if __name__ == '__main__':
    reporter = Reporter()
    database = Database()
    testmanager = TestManager()
    testmanager.setReporter(reporter)
    testmanager.setDB(database)
    reporter.setTM(testmanager)
    database.setTM(testmanager)

    for i in range(3):
        testcontroller = TestController()
        testcontroller.setTM(testmanager)
        testmanager.setTC(testcontroller)
        testcontroller.setup()
        testcontroller.execute()
        testcontroller.tearDown()
